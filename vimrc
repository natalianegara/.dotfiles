autocmd VimEnter * NERDTree
autocmd BufEnter * NERDTreeMirror

autocmd VimEnter * wincmd w

highlight Directory ctermfg=DarkGrey
highlight Comment ctermfg=DarkGrey
syntax enable
